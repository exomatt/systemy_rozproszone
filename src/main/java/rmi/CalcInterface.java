package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalcInterface extends Remote {
    Double sum(Double first, Double second) throws RemoteException;
    Double sub(Double first, Double second) throws RemoteException;
    Double multiplier(Double first, Double second) throws RemoteException;
    String divide(Double first, Double second) throws RemoteException;

    String getDescription(String text) throws RemoteException;


}
