package rmi;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

public class ClientCalc {
    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");

        System.setSecurityManager(new SecurityManager());

        try {
//            Registry registry = LocateRegistry.getRegistry();
            Registry registry = LocateRegistry.getRegistry("145.239.91.137", 1099);
            System.out.println(Arrays.toString(registry.list()));
            CalcInterface myRemoteObject = (CalcInterface) registry.lookup("OBJ");
//            rmi.CalcInterface myRemoteObject = (rmi.CalcInterface) Naming.lookup("//localhost/OBJ");
            if(myRemoteObject==null){
                System.out.println("jednak null");
            }
            System.out.println(myRemoteObject.toString() );
            String text = "Message";

            String result = myRemoteObject.getDescription(text);

            System.out.println("Send to server: " + text);
            System.out.println("Receive result: " + result);

            System.out.println("Send to  sum operation: " );
            Double temp = myRemoteObject.sum(2d,2d);

            System.out.println("Result: " + temp);

            System.out.println("Send to  sub operation: " );
            System.out.println("Result: "+myRemoteObject.sub(2.758, 18.75));

            System.out.println("Send to  multiplier operation: " );
            System.out.println("Result: "+myRemoteObject.multiplier(2.758, 5.0));

            System.out.println("Send to  divide operation: " );
            System.out.println("Result: "+myRemoteObject.divide(2.758, 5.0));

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
