package rmi;

import java.rmi.Naming;

public class Client {
    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");

        System.setSecurityManager(new SecurityManager());

        try {

            CalcInterface myRemoteObject = (CalcInterface) Naming.lookup("//localhost/OBJ");

            String text = "Message";

//            String result = myRemoteObject.getDescription(text);

            System.out.println("Send to server: " + text);

//            System.out.println("Receive result: " + result);

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}