package web1.zad1;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class WebServerClient2 {

    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://localhost:8080/ws/hello?wsdl");
        QName qname = new QName("http://zad1.web1/", "HelloWorldImplService");

        Service service = Service.create(url, qname);

        HelloWorld hello = service.getPort(HelloWorld.class);
        String zapytanie = "To ja klient";
        String response = hello.getHelloWorldAsString(zapytanie);
        System.out.println("klient wysłał " + zapytanie);
        System.out.println("serwer wysłał " + response);

    }

}
