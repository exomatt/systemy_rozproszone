package web1.zad1;


public class WebServerClient {
    public static void main(String[] args) {
        HelloWorldImplService service = new HelloWorldImplService();
        HelloWorld helloWorld = service.getHelloWorldImplPort();
        String zapytanie = "To ja klient";
        String response = helloWorld.getHelloWorldAsString(zapytanie);
        System.out.println("klient wysłał " + zapytanie);
        System.out.println("serwer wysłał " + response);
    }
}
