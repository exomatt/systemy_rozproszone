package rmi2.zad3;

import rmi2.zad2.ChatInt;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GraInt extends Remote {
    public String getName() throws RemoteException;
    public int[][] send(int x, int y, String player) throws RemoteException;
    public void setClient(GraInt c)throws RemoteException;
    public GraInt getClient() throws RemoteException;
    public void newGame() throws RemoteException;
}
