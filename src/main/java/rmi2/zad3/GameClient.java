package rmi2.zad3;

import rmi2.zad2.Chat;
import rmi2.zad2.ChatInt;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import java.util.Scanner;

public class GameClient {
    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");
        System.setProperty("java.rmi.server.hostname", "82.139.170.37");
        System.setSecurityManager(new SecurityManager());

        try {
//            Registry registry = LocateRegistry.getRegistry();
            Registry registry = LocateRegistry.getRegistry("82.139.141.236", 1099);
            System.out.println(Arrays.toString(registry.list()));

            Scanner s=new Scanner(System.in);
            System.out.println("Enter Your name and press Enter:");
            String name=s.nextLine().trim();
            GraInt client = new GraImpl(name);

            GraInt server = (GraInt) registry.lookup("OBJ");
            server.setClient(client);
            while(true){
                System.out.println("[System] Waiting for move:");
                String msg = s.nextLine().trim();
                String[] split = msg.split(";");
                int x = Integer.parseInt(split[0]);
                int y = Integer.parseInt(split[1]);

//                    msg = "[" + server.getName() + "] " + msg;
                int[][] tab = server.send(x, y, name);
                client.send(x, y, name);
//                for (int i = 0; i < tab.length; i++) {
//                    for (int i1 = 0; i1 < tab[0].length; i1++) {
//                        if(tab[i][i1] == 0) System.out.print(" ");
//                        else if(tab[i][i1] == 1) System.out.print("O");
//                        else System.out.print("X");
//                    }
//                    System.out.print("\n");
//                }
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

}
