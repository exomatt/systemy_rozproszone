package rmi2.zad3;

import rmi2.zad2.ChatInt;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class GraImpl extends UnicastRemoteObject implements GraInt {

    public String name;
    public GraInt client = null;

    int[][] tab = new int[3][3];

    protected GraImpl(String name) throws RemoteException {
        this.name = name;
    }

    @Override
    public String getName() throws RemoteException {
        return this.name;
    }

    @Override
    public int[][] send(int x, int y, String player) throws RemoteException {

        int n = 3;
        int moveCount = 0;
        int s = player.matches(name) ? 1 : 2;

        if (tab[x][y] == 0) {
            if (player.matches(name)) tab[x][y] = 1;
            else tab[x][y] = 2;
        }
        moveCount++;

        System.out.print("\033[H\033[2J");
        System.out.flush();

        //check col
        for (int i = 0; i < n; i++) {
            if (tab[x][i] != s)
                break;
            if (i == n - 1) {
                System.out.println("Winner is: " + s);
            }
        }

        //check row
        for (int i = 0; i < n; i++) {
            if (tab[i][y] != s)
                break;
            if (i == n - 1) {
                System.out.println("Winner is: " + s);
            }
        }

        //check diag
        if (x == y) {
            //we're on a diagonal
            for (int i = 0; i < n; i++) {
                if (tab[i][i] != s)
                    break;
                if (i == n - 1) {
                    System.out.println("Winner is: " + s);
                }
            }
        }

        //check anti diag (thanks rampion)
        if (x + y == n - 1) {
            for (int i = 0; i < n; i++) {
                if (tab[i][(n - 1) - i] != s)
                    break;
                if (i == n - 1) {
                    System.out.println("Winner is: " + s);
                }
            }
        }

        //check draw
        if (moveCount == (Math.pow(n, 2) - 1)) {
            System.out.println("Draw");
        }

        for (int i = 0; i < tab.length; i++) {
            for (int i1 = 0; i1 < tab[0].length; i1++) {
                if(tab[i][i1] == 0) System.out.print(" ");
                else if(tab[i][i1] == 1) System.out.print("O");
                else System.out.print("X");
            }
            System.out.print("\n");
        }
        System.out.print("\n");  System.out.print("\n");  System.out.print("\n");  System.out.print("\n");
        return null;
    }

    @Override
    public void setClient(GraInt c) throws RemoteException {
        this.client = c;
    }

    @Override
    public GraInt getClient() throws RemoteException {
        return client;
    }

    @Override
    public void newGame() throws RemoteException {
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[0].length; j++) {
                tab[i][j] = 0;
            }
        }
    }


}
