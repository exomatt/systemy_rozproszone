package rmi2.zad2;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatInt extends Remote {

    public String getName() throws RemoteException;
    public void send(String msg) throws RemoteException;
    public void setClient(ChatInt c)throws RemoteException;
    public ChatInt getClient() throws RemoteException;
}