package rmi2.zad2;

import rmi2.Person;
import rmi2.ServerPersonInterface;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ChatClient {
    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");
        System.setProperty("java.rmi.server.hostname", "82.139.170.37");
        System.setSecurityManager(new SecurityManager());

        try {
//            Registry registry = LocateRegistry.getRegistry();
            Registry registry = LocateRegistry.getRegistry("82.139.141.236", 1099);
//            Registry registry = LocateRegistry.getRegistry("82.139.140.160", 1099);
            System.out.println(Arrays.toString(registry.list()));

            Scanner s=new Scanner(System.in);
            System.out.println("Enter Your name and press Enter:");
            String name=s.nextLine().trim();
            ChatInt client = new Chat(name);

            ChatInt server = (ChatInt) registry.lookup("OBJ");
            String msg="["+client.getName()+"] got connected";
            server.send(msg);
            System.out.println("[System] Chat Remote Object is ready:");
            server.setClient(client);

            while(true){
                msg=s.nextLine().trim();
                msg="["+client.getName()+"] "+msg;
                server.send(msg);
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
