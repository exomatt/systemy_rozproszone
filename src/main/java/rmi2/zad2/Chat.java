package rmi2.zad2;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Chat extends UnicastRemoteObject implements ChatInt {

    public String name;
    public ChatInt client = null;

    public Chat(String n) throws RemoteException {
        this.name = n;
    }

    public String getName() throws RemoteException {
        return this.name;
    }

    public void setClient(ChatInt c) {
        client = c;
    }

    public ChatInt getClient() {
        return client;
    }

    public void send(String s) throws RemoteException {
        System.out.println(s);
    }
}