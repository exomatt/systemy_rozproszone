package rmi2;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ServerPersonInterface extends Remote {

    List<Person> getPersons() throws RemoteException;

}