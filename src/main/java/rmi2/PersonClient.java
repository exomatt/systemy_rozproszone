package rmi2;

import rmi.CalcInterface;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;
import java.util.List;

public class PersonClient {
    public static void main(String[] args) {

        System.setProperty("java.security.policy", "security.policy");

        System.setSecurityManager(new SecurityManager());

        try {
            Registry registry = LocateRegistry.getRegistry();
//            Registry registry = LocateRegistry.getRegistry("145.239.91.137", 1099);
            System.out.println(Arrays.toString(registry.list()));
            ServerPersonInterface myRemoteObject = (ServerPersonInterface) registry.lookup("OBJ");
//            rmi.CalcInterface myRemoteObject = (rmi.CalcInterface) Naming.lookup("//localhost/OBJ");
            if (myRemoteObject == null) {
                System.out.println("jednak null");
            }
            List<Person> persons = myRemoteObject.getPersons();
            persons.forEach(p -> System.out.println(p.toString()));

        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
